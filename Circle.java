public class Circle {
	int radius;
	Point center;
	
	public Circle(int radius,int xCoord,int yCoord) {
		this.radius = radius;
		this.center = new Point(xCoord,yCoord);
	}
	
	public double area() {
		return (Math.PI *(this.radius*this.radius));
	}
	
	public double perimeter() {
		return (2 * Math.PI * this.radius);
	}
	
	public boolean intersect(int radius,int xCoord,int yCoord) {
		Point secondCenter = new Point(xCoord,yCoord);
		double distance;
		distance = Math.sqrt(Math.pow(this.center.xCoord - secondCenter.xCoord,2) + Math.pow(this.center.yCoord - secondCenter.yCoord,2));
		if (distance <= this.radius + radius){
			return true;
		}
		
		else 
		{
			return false;
		}

	}

}
