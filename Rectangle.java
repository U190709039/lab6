public class Rectangle {
	public int sideA;
	public int sideB;
	public Point topLeft;
	
	public Rectangle(int sideA,int sideB,int xCoord,int yCoord) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.topLeft = new Point(xCoord,yCoord);
	}


	public double area() {
		return (this.sideA * this.sideB);
	}
	
	public double perimeter() {
		return 2*(this.sideA + this.sideB);
	}
	
	public  String corners() {
		if (this.sideA <= this.sideB)
		{
			Point topRight = new Point(topLeft.xCoord + this.sideB,topLeft.yCoord);
			Point botLeft = new Point(topLeft.xCoord,topLeft.yCoord - this.sideA);
			Point botRight = new Point(topLeft.xCoord + this.sideB,topLeft.yCoord-this.sideA);
			String A = "A:" + this.topLeft.xCoord +"," +this.topLeft.yCoord;
			String B = " B:" + topRight.xCoord + "," +topRight.yCoord;
			String C = " C:" + botLeft.xCoord + "," + botLeft.yCoord;
			String D = " D:" + botRight.xCoord + "," + botRight.yCoord;
			return A+ B+ C+ D;
			
		}
		
		else 
		{
			Point topRight = new Point(topLeft.xCoord + this.sideA,topLeft.yCoord);
			Point botLeft = new Point(topLeft.xCoord,topLeft.yCoord - this.sideB);
			Point botRight = new Point(topLeft.xCoord + this.sideA,topLeft.yCoord-this.sideB);
			String A = "A: " + this.topLeft.xCoord +"," +this.topLeft.yCoord;
			String B = " B: " + topRight.xCoord + "," +topRight.yCoord;
			String C = " C: " + botLeft.xCoord + "," + botLeft.yCoord;
			String D = " D: " + botRight.xCoord + "," + botRight.yCoord;
			return A+ B+ C+ D;
		}
	}


}

