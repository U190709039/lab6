public class Main {

	public static void main(String[] args) {
		Rectangle myRec;
		myRec= new Rectangle(5,12,5,12);
		System.out.println("Rectangle Question");
		System.out.println("Area of Rectangle:"+ myRec.area());
		System.out.println("Perimeter of Rectangle:" + myRec.perimeter());
		System.out.println("Corners of Rectangle:"+ myRec.corners());
		System.out.println();
		
		Circle myCircle;
		myCircle = new Circle(10,6,8);
		System.out.println("Circle Question:");
		System.out.println("Radius of Circle:"+ myCircle.radius);
		System.out.println("Area of Circle:"+ myCircle.area());
		System.out.println("Perimeter of Circle:"+ myCircle.perimeter());
		System.out.println("Are they Intersect ? "+ myCircle.intersect(10,30,8));
		
	}

}