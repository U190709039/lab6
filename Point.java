public class Point {
	public int xCoord;
	public int yCoord;

	public Point(int xCoord,int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}
		
	public static void main(String[] args) {
	Point aPoint = new Point(5,12);
	System.out.println(aPoint);
	}
}
